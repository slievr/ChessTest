<?php

namespace SolarWinds\Chess;

use SolarWinds\Chess\ChessBoard;
use SolarWinds\Chess\MovementTypeEnum;
use SolarWinds\Chess\Pawn;
use SolarWinds\Chess\PieceColorEnum;

class PawnTest extends \PHPUnit_Framework_TestCase
{

    /** @var  ChessBoard */
    private $_chessBoard;
    /** @var  Pawn */
    private $_testSubject;

    public function setUp()
    {
        $this->_chessBoard = new ChessBoard();
        $this->_testSubject = new Pawn(PieceColorEnum::BLACK());
    }

    public function testChessBoardAddSetsXCoordinate()
    {
        $this->_chessBoard->add($this->_testSubject, 6, 3, PieceColorEnum::WHITE());
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
    }

    public function testChessBoardAddSetsYCoordinate()
    {
        $this->_chessBoard->add($this->_testSubject, 6, 3, PieceColorEnum::WHITE());
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

    public function testPawnMoveIllegalCoordinatesRightDoesNotMove()
    {
        $this->_chessBoard->add($this->_testSubject, 6, 3, PieceColorEnum::WHITE());
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 7, 3);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

    public function testPawnMoveIllegalCoordinatesLeftDoesNotMove()
    {
        $this->_chessBoard->add($this->_testSubject, 6, 3, PieceColorEnum::WHITE());
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 5, 3);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

    public function testPawnMoveLegalCoordinatesForwardUpdatesCoordinates()
    {
        $this->_chessBoard->add($this->_testSubject, 6, 3, PieceColorEnum::WHITE());
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 6, 2);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(2, $this->_testSubject->getYCoordinate());
    }

    public function testPawnMoveLegalCoordinatesInitialForwardUpdatesCoordinates()
    {
        $this->_chessBoard->add($this->_testSubject, 6, 6, PieceColorEnum::WHITE());
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 6, 4);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(4, $this->_testSubject->getYCoordinate());
    }

    public function testPawnMoveLegalCoordinatesBackwardDoesNotMove()
    {
        $this->_chessBoard->add($this->_testSubject, 6, 3, PieceColorEnum::WHITE());
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 6, 4);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

    public function testPawnMoveOccupiedSpaceDoesNotMove()
    {
        $this->_chessBoard->add($this->_testSubject, 6, 3, PieceColorEnum::WHITE());
        $friend = new Pawn(PieceColorEnum::WHITE());
        $this->_chessBoard->add($friend, 6, 2, PieceColorEnum::WHITE());
        $this->_testSubject->move(MovementTypeEnum::MOVE(), 6, 2);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

    public function testPawnCaptureLegalCoordinatesUpdatesCoordinates()
    {
        $this->_chessBoard->add($this->_testSubject, 6, 3, PieceColorEnum::WHITE());
        $oponent = new Pawn(PieceColorEnum::WHITE());
        $this->_chessBoard->add($oponent, 5, 2, PieceColorEnum::BLACK());
        $this->_testSubject->move(MovementTypeEnum::CAPTURE(), 5, 2);
        $this->assertEquals(5, $this->_testSubject->getXCoordinate());
        $this->assertEquals(2, $this->_testSubject->getYCoordinate());
    }

    public function testPawnCaptureIllegalCoordinatesDoesNotMove()
    {
        $this->_chessBoard->add($this->_testSubject, 6, 3, PieceColorEnum::WHITE());
        $oponent = new Pawn(PieceColorEnum::WHITE());
        $this->_chessBoard->add($oponent, 7, 4, PieceColorEnum::BLACK());
        $this->_testSubject->move(MovementTypeEnum::CAPTURE(), 7, 4);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

    public function testPawnCaptureFriendlyDoesNotMove()
    {
        $this->_chessBoard->add($this->_testSubject, 6, 3, PieceColorEnum::WHITE());
        $friend = new Pawn(PieceColorEnum::BLACK());
        $this->_chessBoard->add($friend, 5, 2, PieceColorEnum::WHITE());
        $this->_testSubject->move(MovementTypeEnum::CAPTURE(), 5, 2);
        $this->assertEquals(6, $this->_testSubject->getXCoordinate());
        $this->assertEquals(3, $this->_testSubject->getYCoordinate());
    }

}
