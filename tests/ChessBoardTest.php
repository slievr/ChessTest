<?php

namespace SolarWinds\Chess;

use SolarWinds\Chess\ChessBoard;
use SolarWinds\Chess\Pawn;
use SolarWinds\Chess\PieceColorEnum;

class ChessBoardTest extends \PHPUnit_Framework_TestCase
{

    /** @var  ChessBoard */
    private $_testSubject;

    public function setUp()
    {
        $this->_testSubject = new ChessBoard();
    }

    public function testhasMaxboardwidthOf8()
    {
        $this->assertEquals(8, ChessBoard::MAX_BOARD_WIDTH);
    }

    public function testhasMaxboardheightOf7()
    {
        $this->assertEquals(8, ChessBoard::MAX_BOARD_HEIGHT);
    }

    public function testislegalboardpositionTrueXEquals0YEquals0()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(0, 0);
        $this->assertTrue($isValidPosition);
    }

    public function testislegalboardpositionTrueXEquals5YEquals5()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(5, 5);
        $this->assertTrue($isValidPosition);
    }

    public function testislegalboardpositionFalseXEquals11YEquals5()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(11, 5);
        $this->assertFalse($isValidPosition);
    }

    public function testislegalboardpositionFalseXEquals0YEquals9()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(0, 9);
        $this->assertFalse($isValidPosition);
    }

    public function testIslegalboardpositionFalseXEquals11YEquals0()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(11, 0);
        $this->assertFalse($isValidPosition);
    }

    public function testIsLegalBoardPositionFalseForNegativeYValues()
    {
        $isValidPosition = $this->_testSubject->isLegalBoardPosition(5, -1);
        $this->assertFalse($isValidPosition);
    }

    public function testAvoidsDuplicatePositioning()
    {
        $firstPawn  = new Pawn(PieceColorEnum::BLACK());
        $secondPawn = new Pawn(PieceColorEnum::BLACK());
        $this->_testSubject->add($firstPawn, 6, 3, PieceColorEnum::BLACK());
        $this->_testSubject->add($secondPawn, 6, 3, PieceColorEnum::BLACK());
        $this->assertEquals(6, $firstPawn->getXCoordinate());
        $this->assertEquals(3, $firstPawn->getYCoordinate());
        $this->assertEquals(null, $secondPawn->getXCoordinate());
        $this->assertEquals(null, $secondPawn->getYCoordinate());
    }

    public function testRemovePieceRemovesPiece()
    {
      $firstPawn  = new Pawn(PieceColorEnum::BLACK());
      $this->_testSubject->add($firstPawn, 6, 3, PieceColorEnum::BLACK());
      $this->assertEquals(6, $firstPawn->getXCoordinate());
      $this->assertEquals(3, $firstPawn->getYCoordinate());
      $this->_testSubject->remove($firstPawn, 6, 3, PieceColorEnum::BLACK());
      $this->assertEquals(null, $firstPawn->getXCoordinate());
      $this->assertEquals(null, $firstPawn->getYCoordinate());

    }

    public function testRemoveIncorrectPieceLeavesPiece()
    {
      $firstPawn  = new Pawn(PieceColorEnum::BLACK());
      $this->_testSubject->add($firstPawn, 6, 3, PieceColorEnum::BLACK());
      $this->assertEquals(6, $firstPawn->getXCoordinate());
      $this->assertEquals(3, $firstPawn->getYCoordinate());
      $secondPawn = new Pawn(PieceColorEnum::WHITE());
      $this->_testSubject->remove($secondPawn, 6, 3, PieceColorEnum::BLACK());
      $this->assertEquals(6, $firstPawn->getXCoordinate());
      $this->assertEquals(3, $firstPawn->getYCoordinate());

    }

    public function testlimitsTheNumberOfPawns()
    {
        for ($i = 0; $i < 10; $i++) {
            $pawn = new Pawn(PieceColorEnum::BLACK());
            $row  = $i / ChessBoard::MAX_BOARD_WIDTH;
            $this->_testSubject->add($pawn, floor(6 + $row), $i % ChessBoard::MAX_BOARD_WIDTH, PieceColorEnum::BLACK());
            if ($row < 1) {
                $this->assertEquals(floor(6 + $row), $pawn->getXCoordinate());
                $this->assertEquals($i % ChessBoard::MAX_BOARD_WIDTH, $pawn->getYCoordinate());
            } else {
                $this->assertEquals(null, $pawn->getXCoordinate());
                $this->assertEquals(null, $pawn->getYCoordinate());
            }
        }
    }

    public function testlimitsTheNumberOfPawnsPerColour()
    {

        for ($i = 0; $i < 10; $i++) {
            $pawn = new Pawn(PieceColorEnum::WHITE());
            $row  = $i / ChessBoard::MAX_BOARD_WIDTH;
            $this->_testSubject->add($pawn, floor(1 + $row), $i % ChessBoard::MAX_BOARD_WIDTH, PieceColorEnum::WHITE());
            if ($row < 1) {
                $this->assertEquals(floor(1 + $row), $pawn->getXCoordinate());
                $this->assertEquals($i % ChessBoard::MAX_BOARD_WIDTH, $pawn->getYCoordinate());
            } else {
                $this->assertEquals(null, $pawn->getXCoordinate());
                $this->assertEquals(null, $pawn->getYCoordinate());
            }
        }

        for ($i = 0; $i < 10; $i++) {
            $pawn = new Pawn(PieceColorEnum::BLACK());
            $row  = $i / ChessBoard::MAX_BOARD_WIDTH;
            $this->_testSubject->add($pawn, floor(6 + $row), $i % ChessBoard::MAX_BOARD_WIDTH, PieceColorEnum::BLACK());
            if ($row < 1) {
                $this->assertEquals(floor(6 + $row), $pawn->getXCoordinate());
                $this->assertEquals($i % ChessBoard::MAX_BOARD_WIDTH, $pawn->getYCoordinate());
            } else {
                $this->assertEquals(null, $pawn->getXCoordinate());
                $this->assertEquals(null, $pawn->getYCoordinate());
            }
        }

    }

}
