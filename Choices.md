# Choices

This outlines some desing choices and why they were made for the system going forward

## board size

size was increased to 8x8 to match an actual chess board.

## initial move

When a pawn initially moves it has the abillity to move 2 spaces rather than the usual one.

## docker

Docker is used for development and cross compatibillity.
live reloading can be used for development using `docker-compose -f docker-hot-reload.yml up --build`

tests can also be run using `docker build -t chess . && docker run -it chess`

## abstract piece

An abstract piece class has been implemented to allow for different pieces to be added after

## MAX_PIECE_TYPE

Max piece type is stored as an associative array so that in future extra logic doesn't need added to determine maximum number of a specific piece tpye, per colour.

This is also stored on a `ChessBoard` rather than on pieces as in future there may be subclasses of boards to deal with chess problems where more complicated piece arrangements are required. For instance a king surrounded by 10 pawns, or a 3 rooks.
