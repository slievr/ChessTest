#!/bin/bash
inotifywait -m -r --exclude .git -e close_write src tests |
while read -r; do
  ./vendor/bin/phpunit -v --colors=always --bootstrap vendor/autoload.php tests
done
