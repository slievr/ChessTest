<?php

namespace SolarWinds\Chess;

class Pawn extends Piece
{
    public function move(MovementTypeEnum $movementTypeEnum, $newX, $newY): Piece
    {
        $oldX = $this->getXCoordinate();
        $oldY = $this->getYCoordinate();

        if ($movementTypeEnum == MovementTypeEnum::MOVE()) {
            return $this->moveTo($newX, $newY);
        } else {
            return $this->capture($newX, $newY);
        }
    }

    public function moveTo($newX, $newY): Pawn
    {

        $chessBoard = $this->getChessBoard();
        $piece      = $chessBoard->getPieceAtPosition($newX, $newY);
        $moveRange  = $this->isStartingRow() ? 2 : 1;

        if (!$chessBoard || $piece) {
            return $this;
        }

        if ($this->isValidMove($newX, $newY, $moveRange)) {
            $chessBoard->remove($this, $newX, $newY, $this->getPieceColor());
            $chessBoard->add($this, $newX, $newY, $this->getPieceColor());
        }

        return $this;
    }

    private function isValidMove($newX, $newY, $moveRange): bool
    {
        if ($this->getPieceColor() == PieceColorEnum::WHITE()) {
            if (
                ($newY > $this->getYCoordinate())
                && (abs($newY - $this->getYCoordinate()) <= $moveRange)
                && ($newX == $this->getXCoordinate())
            ) {
                return true;
            }
        } else {
            if (
                ($newY < $this->getYCoordinate())
                && (abs($newY - $this->getYCoordinate()) <= $moveRange)
                && ($newX == $this->getXCoordinate())
            ) {
                return true;
            }
        }

        return false;
    }

    public function capture($newX, $newY): Pawn
    {

        $chessBoard = $this->getChessBoard();
        $piece      = $chessBoard->getPieceAtPosition($newX, $newY);

        if ($this->isValidCapture($newX, $newY) && $this->isCaptureTarget($piece)) {
            $chessBoard->remove($piece, $newX, $newY, $this->getPieceColor());
            $chessBoard->add($this, $newX, $newY, $this->getPieceColor());
        }

        return $this;
    }

    public function isStartingRow(): bool
    {
        if ($this->getPieceColor() == PieceColorEnum::BLACK() && $this->getYCoordinate() == 6) {
            return true;
        }

        if ($this->getPieceColor() == PieceColorEnum::WHITE() && $this->getYCoordinate() == 1) {
            return true;
        }

        return false;
    }

    public function maxPlacesToMove(): int
    {
        if ($this->isStartingRow()) {
            return 2;
        } else {
            return 1;
        }
    }

    private function isCaptureTarget($piece): bool
    {
        if ($piece && ($piece->getPieceColor() != $this->getPieceColor())) {
            return true;
        }

        return false;
    }

    private function isValidCapture($newX, $newY): bool
    {
        if ($this->getPieceColor() == PieceColorEnum::WHITE()) {
            if (
                ($newY == ($this->getYCoordinate() + 1) && (abs($newX - $this->getXCoordinate()) == 1))
            ) {
                return true;
            }
        } else {
            if (
                ($newY == ($this->getYCoordinate() - 1) && (abs($newX - $this->getXCoordinate()) == 1))
            ) {
                return true;
            }
        }

        return false;
    }
}
