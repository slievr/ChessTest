<?php

namespace SolarWinds\Chess;

class ChessBoard
{
    const MAX_BOARD_WIDTH  = 8;
    const MAX_BOARD_HEIGHT = 8;
    const MAX_PIECE_TYPE   = [
        "Pawn" => 8,
    ];

    private $_pieces;

    public function __construct()
    {
        $this->_pieces = array_fill(0, self::MAX_BOARD_WIDTH, array_fill(0, self::MAX_BOARD_HEIGHT, null));
    }

    public function add(Piece $piece, $xCoordinate, $yCoordinate, PieceColorEnum $pieceColor): ChessBoard
    {
        if (!$this->isMaxPieceType($piece) && $this->isLegalBoardPosition($xCoordinate, $yCoordinate)) {
            $piece->setChessBoard($this);
            $piece->setXCoordinate($xCoordinate);
            $piece->setYCoordinate($yCoordinate);
            $this->_pieces[$xCoordinate][$yCoordinate] = $piece;
        }

        return $this;
    }

    public function remove(Piece $piece, $xCoordinate, $yCoordinate, PieceColorEnum $pieceColor): ChessBoard
    {
        $boardPiece = $this->getPieceAtPosition($xCoordinate, $yCoordinate);
        if ($boardPiece === $piece) {
            $piece->setXCoordinate(null);
            $piece->setYCoordinate(null);
            $this->_pieces[$xCoordinate][$yCoordinate] = null;
        }

        return $this;
    }

    /**
     * @return boolean
     **/
    public function isMaxPieceType(Piece $piece): bool
    {
        $pieceCount = array_reduce(
            $this->_pieces,
            function ($accumulator, $row) use ($piece) {
                foreach ($row as $square) {
                    if ($square !== null) {
                        if ((get_class($square) === get_class($piece)) && ($square->getPieceColor() == $piece->getPieceColor())) {
                            $accumulator++;
                        }
                    }
                }
                return $accumulator;
            },
            0
        );

        return $pieceCount < self::MAX_PIECE_TYPE[$piece->getType()] ? false : true;
    }

    /**
     * @return boolean
     **/

    public function getPieceAtPosition($xCoordinate, $yCoordinate): ?Piece
    {
        if ($this->isWithinBoardConstraints($xCoordinate, $yCoordinate)) {
            return $this->_pieces[$xCoordinate][$yCoordinate];
        } else {
            throw new \Exception("can't get piece outside board constraints");
        }
    }

    /**
     * @return boolean
     **/
    public function isPositionTaken($xCoordinate, $yCoordinate): bool
    {
        if ($this->getPieceAtPosition($xCoordinate, $yCoordinate) != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return boolean
     **/
    public function isWithinBoardConstraints($xCoordinate, $yCoordinate): bool
    {
        if ($xCoordinate < 0 || $xCoordinate > self::MAX_BOARD_WIDTH) {

            return false;
        }

        if ($yCoordinate < 0 || $yCoordinate > self::MAX_BOARD_HEIGHT) {
            return false;
        }

        return true;
    }

    /**
     * @return boolean
     **/
    public function isLegalBoardPosition($xCoordinate, $yCoordinate): bool
    {

        if (!$this->isWithinBoardConstraints($xCoordinate, $yCoordinate)) {
            return false;
        }

        if ($this->isPositionTaken($xCoordinate, $yCoordinate)) {
            return false;
        }

        return true;
    }
}
