<?php

namespace SolarWinds\Chess;

abstract class Piece
{
    /** @var PieceColorEnum */
    private $_pieceColorEnum;

    /** @var ChessBoard */
    private $_chessBoard;

    /** @var int */
    private $_xCoordinate;

    /** @var int */
    private $_yCoordinate;

    public function __construct(PieceColorEnum $pieceColorEnum)
    {
        $this->pieceColorEnum = $pieceColorEnum;
    }

    public function getChessBoard(): ?Chessboard
    {
        return $this->chessBoard;
    }

    public function setChessBoard(ChessBoard $chessBoard)
    {
        $this->chessBoard = $chessBoard;
    }

    /** @return int */
    public function getXCoordinate(): ?int
    {
        return $this->xCoordinate;
    }

    /** @var int */
    public function setXCoordinate($value)
    {
        $this->xCoordinate = $value;
    }

    /** @return int */
    public function getYCoordinate(): ?int
    {
        return $this->yCoordinate;
    }

    /** @var int */
    public function setYCoordinate($value)
    {
        $this->yCoordinate = $value;
    }

    public function getPieceColor(): ?PieceColorEnum
    {
        return $this->pieceColorEnum;
    }

    public function setPieceColor(PieceColorEnum $value)
    {
        $this->pieceColorEnum = $value;
    }

    public function move(MovementTypeEnum $movementTypeEnum, $newX, $newY): Piece
    {
        return $this;
    }

    public function getType(): String
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    public function toString()
    {
        return "x({$this->xCoordinate}), y({$this->yCoordinate}), pieceColor({$this->pieceColorEnum->toString()}), type({$this->get_type()})";
    }
}
